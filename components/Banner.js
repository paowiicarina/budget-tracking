
import { Jumbotron, Row, Col } from 'react-bootstrap';
import Link from 'next/link';
import PropTypes from 'prop-types';

export default function Banner({dataProp}){
	const {title, content, destination, label} = dataProp;
	const divStyle = {
		backgroundImage: 'url(' + `https://images.unsplash.com/photo-1516796181074-bf453fbfa3e6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60` + ')',
	};


	return(
		<Row>
			<Col>
				<Jumbotron style={divStyle}>
					<h1 className="w-100 text-center d-flex justify-content-center">{title}</h1>
					<p className="w-100 text-center d-flex justify-content-center">{content}</p>
					<Link href={destination}>
						<a className="w-100 text-center d-flex justify-content-center text-dark">{label}</a>
					</Link>
				</Jumbotron>
			</Col>
		</Row>
	)
}

Banner.propTypes = {
	data: PropTypes.shape({
		title: PropTypes.string.isRequired,
		content: PropTypes.string.isRequired,
		destination: PropTypes.string.isRequired,
		label: PropTypes.string.isRequired
	})
}