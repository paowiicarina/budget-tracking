import { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../UserContext';

export default function NavBar(){
	const { user } = useContext(UserContext);
	
    const navbar = {backgroundColor: '#E4B4B4'}
  
return(
       <Navbar style={navbar} expand="lg">
            <Link href="/">
                <a className="navbar-brand">Thiftified</a>
            </Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">

                {console.log(user)}
                {(user.email !== null)
                    ? <>
                    <div class="ml-auto text-right container-fluid text">
                        <nav>
                            <ul class="navbar-nav">
                                <Link href="/categories">
                                <a className="nav-link" role="button">Categories</a>
                                </Link>
                                <Link href="/records">
                                    <a className="nav-link" role="button">Records</a>
                                </Link>
                                <Link href="/analytics">
                                    <a className="nav-link" role="button">Analytics</a>
                                </Link>
                                <Link href="/logout">
                                    <a className="nav-link" role="button">Logout</a>
                                </Link>
                            </ul>
                        </nav>
                    </div>
                        
                    </>
                    : <>
                        <Link href="/Log In">
                                    <a className="nav-link" role="button">Log In</a>
                                </Link>
                        <Link href="/register">
                            <a className="nav-link" role="button">Register</a>
                        </Link> 
                    </>
                }

             </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}





