import { Row, Col, Card } from 'react-bootstrap';


export default function Highlights(){
	const elementType = {
		backgroundImage: 'url(' + `https://images.unsplash.com/photo-1487147264018-f937fba0c817?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60` + ')',
	};

	return(
		<Row>
	        <Col>
	            <Card className="cardHighlight" style={elementType} border="info">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Budgeting Tips for Your Daily Life</h2>
	                    </Card.Title>
	                    <Card.Text>
	                    	 -Budget to zero before the month begins.<br/><br/>
	                    	 -Do the budget together.<br/><br/>
	                    	 -Use the Right Tools<br/><br/>
	                    	 -Keep Bills and Receipts Organized.<br/><br/>
	                    	 -Use Separate Accounts.<br/><br/>
	                    	 -Prioritize Debt Repayment.<br/><br/>
	                    	 Start Contributing to Retirement Now.<br/><br/>
	                    	 -Outline Specific, Realistic Goals.<br/><br/>
	                    	 -Ditch Food Delivery and Cook at Home.<br/><br/>
	                    	 -Consider the 50/20/30 rule, which allocates approximately 30 percent of your income to non-essential things that will enhance your lifestyle.

	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col>
	            <Card className="cardHighlight" style={elementType} border="info">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>3 Simple Steps to Prepare Your Bank Account for Life After Quarantine</h2>
	                    </Card.Title>
	                    <Card.Text>
	                    	COVID-19 has given everyone around the world both time and space to reflect on what individually matters most in life. Whether it’s the chance to walk around outside, or the opportunity to spend more time with your kids, knowing what brought you joy during quarantine can help you learn to spend your money in ways that maximize your happiness. Equally important is knowing what didn’t bring joy to help you avoid wasting time and money.<br/>

								Here are three simple steps you can take to hold onto joy and let go of what brings you down as we all prepare to live our lives after quarantine:<br/>
								<br/>
								Step 1: Rate your spending categories.<br/>
								Step 2: Review your spending and add up your totals.<br/>
								Step 3: Commit to a budget that adds more joy to your life.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col>
	            <Card className="cardHighlight" style={elementType} border="info">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>50/30/20 Budgeting Rule:</h2>
	                    </Card.Title>
	                    <Card.Text>
	                    		Popularized by Senator Elizabeth Warren and her daughter, the 50/30/20 budgeting rule–also referred to as the 50/20/30 budgeting rule–divides after-tax income into three different buckets:<br/><br/>

									Essentials (50%)<br/>
									Wants (30%)<br/>
									Savings (20%)<br/>

									Ask Yourself: Why is a 50/30/20 Budget Necessary?
									<br/>
									There are plenty of different reasons why people start a budget:<br/><br/>

									-To save up for a large expense such as a house, car, or vacation.<br/>
									-Put a security deposit on an apartment.<br/>
									-To reduce spending habits.<br/>
									-To improve credit score.<br/> 
									-To eliminate debt.<br/>
									-To break the paycheck to paycheck cycle.<br/><br/>
									Identifying the reason why you’re budgeting with the 50/30/20 method can help you stay motivated and create a better plan to reach your goal. I
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>			
		</Row>
	)
}
