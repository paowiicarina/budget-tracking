import { useState, useContext } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login'
import Router from 'next/router'
import Swal from 'sweetalert2'
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper'

import Head from 'next/head'


export default function index() {

    return (
       
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3 className="w-100 text-center d-flex justify-content-center">Login</h3>
                    <LoginForm/>
                </Col>
            </Row>
    )
}

const LoginForm = () => {
    const { user, setUser } = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [tokenId, setTokenId] = useState(null)

    const authenticate = (e) => {
        e.preventDefault()

        fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password,
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data.accessToken){
                localStorage.setItem('token', data.accessToken);
                
                fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/details`, {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    } 
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    setUser({
                        id: data._id
                    })
                    Router.push('/records')
                })
            }
        })
    }

    const authenticate2 = (e) => {
        e.preventDefault()

        const payload = {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json'  
            },
            body: JSON.stringify({ 
                email: email, 
                password: password 
            })
        }
        
        fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/login`, payload)
        .then(res => res.json())
        .then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'does-not-exist') {
                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'incorrect-password') {
                    Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }
        })
    }
    
    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
        }

        fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/details`, options)
        .then(res => res.json())
        .then(data => {
            setUser({ email: data.email })
            Router.push('/records')
        })
    }

    const authenticateGoogleToken = (response) => {

        setTokenId(response.tokenId)

        const payload = {
            method: 'post',
            headers: { 'Content-Type': 'application/json' 
        },
            body: JSON.stringify({ 
                tokenId: response.tokenId 
            })
        }

        fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/verify-google-token-id`, payload)
        .then(res => res.json())
        .then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'google-auth-error') {
                    Swal.fire('Google Auth Error', 'Google authentication procedure failed, try again or contact web admin.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }
        })
    }

    return (
        <div>
            <Head>
                <title>Login</title>
            </Head>
            <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label className="w-100 text-center d-flex justify-content-center">Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label className="w-100 text-center d-flex justify-content-center">Password</Form.Label>
                    <Form.Control type="password" placeholder="XXXXXXXXXXXXXX" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>

                <Button variant="warning" type="submit" className="w-100 text-center d-flex justify-content-center">
                    Submit
                </Button>
            </Form><br/><br/>
            
            <GoogleLogin onClick={ authenticate2 }
                clientId="596826335106-km1g2ceti3s83o1aoclhkld576qm650r.apps.googleusercontent.com"
                buttonText="Login"
                onSuccess={ authenticateGoogleToken }
                onFailure={ authenticateGoogleToken }
                cookiePolicy={ 'single_host_origin' }
                className="w-100 text-center d-flex justify-content-center"
            />
    
        </div>
    )
}

        
        