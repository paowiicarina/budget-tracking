import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container  } from 'react-bootstrap';
import Router from 'next/router'
import UserContext from '../../UserContext';

export default function create() {

    let token = ""
    
    const [categoryName, setCategoryName] = useState('');
    const [categoryType, setCategoryType] = useState('');
    const { user, setUser } = useContext(UserContext)

    useEffect(() => {
        token = localStorage.getItem('token')
    },[categoryName, categoryType])
    

    function createCategory(e) {
        e.preventDefault();

        console.log(`${categoryName} is a category with a type: ${categoryType}.`);

        setCategoryName('');
        setCategoryType('');

        fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/add-category`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
                name: categoryName,
                typeName: categoryType
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(categoryType)
            Router.push('/categories')
        })
    }

    

    return (
        <Container>
            <Form onSubmit={(e) => createCategory(e)}>
                <Form.Group controlId="categoryName">
                    <Form.Label className="w-100 text-center d-flex justify-content-center">Category Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter category name" value={categoryName} onChange={e => setCategoryName(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="categoryType">
                    <Form.Label className="w-100 text-center d-flex justify-content-center">Category Type</Form.Label>
                    <Form.Control as="select" type="text" placeholder="Select category type" onClick={e => setCategoryType(e.target.value)} required>
                        <option value="Income" disabled>Select</option>
                        <option value="Income">Income</option>
                        <option value="Expense">Expense</option>
                    </Form.Control>
                </Form.Group>
            

                <Button variant="warning" type="submit" className="w-100 text-center d-flex justify-content-center">Add Category</Button>
            </Form>
        </Container>
    )
}
