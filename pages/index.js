import { useState, useContext } from 'react';
import { Card, Row, Col, Button, Container } from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login'
import Router from 'next/router'
import Swal from 'sweetalert2'
import Link from 'next/link'
import Banner from './../components/Banner';  
import Highlights from './../components/Highlights'; 

export default function Home() {

  const data = {
    title: "Thriftified",
    content: "Your daily expense tracker",
    destination: "/login",
    label: "Let's get started!"
  }

  return (
    <React.Fragment>
      <Container fluid>
        <Banner dataProp={data}/>
      </Container>
        <Container fluid>
        <Highlights/>
      </Container>
    </React.Fragment>
  )
}
