import { useState, useEffect } from 'react';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { UserProvider } from '../UserContext';
import AppHelper from '../app-helper';
import Navbar from '../components/NavBar'
import { Container } from 'react-bootstrap'

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
		email: null
	})

	useEffect(() => {
		const accessToken  = localStorage.getItem('token')

		const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
		}
		
		fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/details`, options)
		.then(res => res.json())
        .then(data => {
			console.log(data)
			if(data.email){
				setUser({ email: data.email, userId: data.userId })
			}else{
				setUser({
					email: null
				})
			}	
		})
		      
	}, [user.email])

	const unsetUser = () => {
		localStorage.clear();

		setUser({
			email: null
		})
	}

  return(
  	<UserProvider value={{user, setUser, unsetUser}}>
	  <Navbar />
	          <Container>
	  	<Component {...pageProps} />
	  		  </Container>
  	</UserProvider>
  )
}

export default MyApp



