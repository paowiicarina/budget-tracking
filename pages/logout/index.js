
import { useContext } from 'react';
import UserContext from '../../UserContext';
import Router from 'next/router';
import { Row, Col, Card, Button } from 'react-bootstrap'



export default function logout(){
	
	const {unsetUser, setUser} = useContext(UserContext)

	function logoutClick(){
		unsetUser();
		Router.push('/login')
	}

	return (
		
			<Row className="justify-content-center">
				<Col xs md="6">
					<h3 className="w-100 text-center d-flex justify-content-center">Are you sure you want to logout?</h3>
					<Card>
						<Card.Header className="w-100 text-center d-flex justify-content-center">Login</Card.Header>
						<Card.Body>
							<Button className="mb-2" variant="warning" type="submit" onClick={ logoutClick } block>Logout</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row> 
	)
}
