import { useState, useEffect } from 'react';
import { Row, Col, Card, Button, ListGroup, Form } from 'react-bootstrap'
import { useContext } from 'react';
import UserContext from '../../UserContext';
import Router from 'next/router';
import Link from 'next/link'
import moment from 'moment'
import BarChartIncome from '../../components/BarChartIncome'
import BarChartExpense from '../../components/BarChartExpense'
import LineChartBalance from '../../components/LineChartBalance'



export default function trends(){
	let token = ""
	const { user, setUser } = useContext(UserContext)

	const [incomeData, setIncomeData] = useState([])
	const [expenseData, setExpenseData] = useState([])
	const [balance, setBalance] = useState([])

	useEffect(() => {
		token = localStorage.getItem('token')

		function handleBalance(){
			fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/get-most-recent-records`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					userId: user.userId
				})
			})
			.then(res => res.json())
			.then(data => {
				
				const balance = data.map(data =>{
					return data
				})
	
				setBalance(balance)
			})
		}

		function uploadIncomeData(){
			fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/get-most-recent-records`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					userId: user.userId
				})
			})
			.then(res => res.json())
			.then(data => {
				const income = data.filter(function (data){
					return data.type === "Income"
				})
		
				setIncomeData(income)
			})
		}
	
		
	
		function uploadExpenseData(){
			fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/get-most-recent-records`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					userId: user.userId
				})
			})
			.then(res => res.json())
			.then(data => {
				const expense = data.filter(function (data){
					return data.type === "Expense"
				})
		
				setExpenseData(expense)
			})
		}

		handleBalance()
		uploadIncomeData()
		uploadExpenseData()
	},[user])
	

	
	
    return (
		<React.Fragment>
			<BarChartIncome rawData={incomeData}/>
			<BarChartExpense rawData={expenseData}/>
			<LineChartBalance rawData={balance}/>
		</React.Fragment>
    )
}

