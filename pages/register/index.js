import {useState, useEffect} from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import Router from 'next/router'
import Head from 'next/head'

export default function index() {

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [isActive, setIsActive] = useState(false)


    useEffect(() => {
         if(name !== '' && email !== '' && password1 !== ''&& password2 !== '' && password1 === password2){
       setIsActive(true);
   }else{
       setIsActive(false)
   }
 }, [name, email, password1, password2])


    function registerUser(e){
        e.preventDefault();

         fetch(`http://localhost:4000/api/users/`,{
                method: 'POST',
                headers: {
                       'Content-Type': 'application/json'
                        },
                 body: JSON.stringify({
                        name: name,
                        email: email,
                        password: password1
                    })
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    if(data === true){
                         Router.push('/login')
                    }else{
                        alert("Registration failed")
                    }
                })
            }

    return (
    	<Container>
	    	<Form onSubmit={(e) => registerUser(e)}>
            <h3 className="w-100 text-center d-flex justify-content-center">Register</h3>
                  <Form.Group controlId="userName">
                    <Form.Label className="w-100 text-center d-flex justify-content-center">Full Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter name" value={name} onChange={e => setName(e.target.value)} required/>
                </Form.Group>
	            <Form.Group controlId="userEmail">
	                <Form.Label className="w-100 text-center d-flex justify-content-center">Email address</Form.Label>
	                <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
	                <Form.Text className="text-muted w-100 text-center d-flex justify-content-center">
	                We'll never share your email with anyone else.
	                </Form.Text>
	            </Form.Group>

	            <Form.Group controlId="password1">
	                <Form.Label className="w-100 text-center d-flex justify-content-center">Password</Form.Label>
	                <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
	            </Form.Group>

	            <Form.Group controlId="password2">
	                <Form.Label className="w-100 text-center d-flex justify-content-center">Verify Password</Form.Label>
	                <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
	            </Form.Group>

	            {isActive ?
	                <Button variant="warning" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center">Submit</Button>
	                :
	                <Button variant="warning" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center" disabled>Submit</Button>
	            }
	        </Form>
    	</Container>
    )
}