import { useState, useEffect } from 'react';
import { Row, Col, Card, Button, ListGroup, Form } from 'react-bootstrap'
import { useContext } from 'react';
import UserContext from '../../UserContext';
import Router from 'next/router';
import Link from 'next/link'
import moment from 'moment'
import BarChartIncome from '../../components/BarChartIncome'
import BarChartExpense from '../../components/BarChartExpense'




import AppHelper from '../../app-helper'



export default function record({ usersData }){
  let token = ""
  
  const [records, setRecords] = useState([])
  const [searchBar, setSearchBar] = useState("")
  const [typeFilter, setTypeFilter] = useState("")
  const [recordsList, setRecordsList] = useState("")

  const { user, setUser } = useContext(UserContext)

  useEffect(() => {
    token = localStorage.getItem('token')

    fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/get-most-recent-records`, {
      method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`
      },
      body: JSON.stringify({
        userId: user.userId
            })
    })
    .then(res => res.json())
    .then(data => {
      setRecordsList(data.map(data => {
        // console.log(data)
        return (
          <ListGroup.Item key={data._id}>
            <b>{data.description}</b>
            <br/>
            {data.type}({data.categoryName})
            <br/>
            {data.type === "Income"
            ? 
              <div className="text-success">
                + {data.amount}
                <br/>
                <b>+ {data.balanceAfterTransaction}</b>
              </div>
            
            : <div className="text-danger">
                - {data.amount}
                <br/>
                <b>- {data.balanceAfterTransaction}</b>
              </div>
    
            }
            
            <br/>
            {moment(data.dateAdded).format('MMM Do YY')}
          </ListGroup.Item>
        )
      }))
    })

    
  }, [])
  
  useEffect(() => {
    token = localStorage.getItem('token')

    fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/search-record`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
        userId: user.userId,
        searchKeyword: searchBar,
        searchType: typeFilter
            })
        })
        .then(res => res.json())
        .then(data => {
      console.log(data)
      setRecordsList(data.map(data => {


    return (
          <ListGroup.Item key={data._id}>
            <b>{data.description}</b>
            <br/>
            {data.type}({data.categoryName})
            <br/>
            {data.type === "Income"
            ? 
              <div className="text-success">
                + {data.amount}
                <br/>
                <b>+ {data.balanceAfterTransaction}</b>
              </div>
            
            : <div className="text-danger">
                - {data.amount}
                <br/>
                <b>- {data.balanceAfterTransaction}</b>
              </div>
    
            }
            
            <br/>
            {moment(data.dateAdded).format('MMM Do YY')}
          </ListGroup.Item>
        )
      }))
        })
  }, [searchBar, typeFilter])

    return (
    <React.Fragment>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <Link href="/records/newrec">
        <a className="btn" role="button">Add</a>
      </Link>
      <Form.Group controlId="recordDesc">
                    <Form.Control type="text" placeholder="Search record" value={searchBar} onChange={e => setSearchBar(e.target.value)} required/>
                </Form.Group>
      <ListGroup>
      <Form.Group controlId="categoryType">
        <Form.Control as="select" type="text" defaultValue="All" onClick={e => setTypeFilter(e.target.value)} required>
          <option value="All">All</option>
          <option value="Income">Income</option>
          <option value="Expense">Expense</option>
        </Form.Control>
      </Form.Group>
        {recordsList}
      </ListGroup>
    </React.Fragment>
    )
}
