import { useState, useEffect } from 'react';
import { Form, Button, Container  } from 'react-bootstrap';
import Router from 'next/router'
import Swal from 'sweetalert2'

export default function create() {
    
    let token = ""

    const [categories, setCategories] = useState([])
    const [newCat, setNewCat] = useState([])
    const [categoryName, setCategoryName] = useState('');
    const [categoryType, setCategoryType] = useState('');
    const [recordAmount, setRecordAmount] = useState(0);
    const [recordDesc, setRecordDesc] = useState('');
    const [balBefore, setBalBefore] = useState(0);
    const [balAfter, setBalAfter] = useState(0);
   

    useEffect(() => {
        token = localStorage.getItem('token')
    }, [categoryName, categoryType, recordAmount, recordDesc, balAfter])

    function createRecord(e) {
        e.preventDefault();

        if(categoryType === "Income"){
            setBalAfter(parseInt(balBefore) + parseInt(recordAmount))
            
        }else if(categoryType === "Expense"){
            setBalAfter(parseInt(balBefore) - parseInt(recordAmount))
            
        }

        setBalBefore(parseInt(balAfter))

        console.log(`${categoryName} is a category with a type: ${categoryType}.`);

        setCategoryName('');
        setCategoryType('');

        fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/add-record`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
                categoryName: categoryName,
                typeName: categoryType,
                amount: recordAmount,
                description: recordDesc,
                balanceAfterTransaction: balAfter
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(categoryType)
            Router.push('/records')
        })
    }


    useEffect(() => {
        const fetchData = async () => {
            try{
                const result = await fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/get-categories`, {
                    method: 'POST',
                    headers: {
                        Authorization: `Bearer ${token}`
                    } 
                })
                .then(res => res.json())
                .then(data => {
              
                    setCategories(data)             
                })
            } catch (err) {
               
            }
        }

        fetchData()
    }, [])
    
    useEffect(() => {
    
        const newCat = categories.filter(function (category){
            return category.type === categoryType
        })

        setNewCat(newCat)
        
    }, [categoryType])

    console.log(newCat)

    return (
        <Container>
        <h3 className="w-100 text-center d-flex justify-content-center">New Record</h3>
            <Form onSubmit={(e) => createRecord(e)}>
                <Form.Group controlId="categoryType">
                    <Form.Label className="w-100 text-center d-flex justify-content-center">Category Type</Form.Label>
                    <Form.Control as="select" type="text" onClick={e => setCategoryType(e.target.value)} required>
                        <option value="Income">Income</option>
                        <option value="Expense">Expense</option>
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId="categoryType">
                    <Form.Label className="w-100 text-center d-flex justify-content-center">Category Name</Form.Label>
                    <Form.Control as="select" type="text" 
                    onClick={e => setCategoryName(e.target.value)} required>
                        {(categoryType !== '')
                        ?   newCat.map(cat => {
                                return(
                                    <option>{cat.name}</option>
                                )
                            })
                        : null
                        }
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId="num1">
                    <Form.Label className="w-100 text-center d-flex justify-content-center">Amount</Form.Label>
                    <Form.Control type="number" value={recordAmount} 
                    onChange={e => setRecordAmount(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="recordDesc">
                    <Form.Label className="w-100 text-center d-flex justify-content-center">Description</Form.Label>
                    <Form.Control type="text" placeholder="Enter record description" value={recordDesc} onChange={e => setRecordDesc(e.target.value)} required/>
                </Form.Group>
                <Button variant="warning" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center">Add Record</Button>
            </Form>
        </Container>
    )
}

