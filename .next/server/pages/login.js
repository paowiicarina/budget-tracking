module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/login/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./UserContext.js":
/*!************************!*\
  !*** ./UserContext.js ***!
  \************************/
/*! exports provided: UserProvider, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"UserProvider\", function() { return UserProvider; });\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n //create a Context Object\n\nconst UserContext = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createContext(); //Provider component that allows consuming components to subscribe to context changes\n\nconst UserProvider = UserContext.Provider;\n/* harmony default export */ __webpack_exports__[\"default\"] = (UserContext);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9Vc2VyQ29udGV4dC5qcz84NmIxIl0sIm5hbWVzIjpbIlVzZXJDb250ZXh0IiwiUmVhY3QiLCJjcmVhdGVDb250ZXh0IiwiVXNlclByb3ZpZGVyIiwiUHJvdmlkZXIiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0NBRUE7O0FBQ0EsTUFBTUEsV0FBVyxnQkFBR0MsNENBQUssQ0FBQ0MsYUFBTixFQUFwQixDLENBRUE7O0FBQ08sTUFBTUMsWUFBWSxHQUFHSCxXQUFXLENBQUNJLFFBQWpDO0FBRVFKLDBFQUFmIiwiZmlsZSI6Ii4vVXNlckNvbnRleHQuanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuXG4vL2NyZWF0ZSBhIENvbnRleHQgT2JqZWN0XG5jb25zdCBVc2VyQ29udGV4dCA9IFJlYWN0LmNyZWF0ZUNvbnRleHQoKTtcblxuLy9Qcm92aWRlciBjb21wb25lbnQgdGhhdCBhbGxvd3MgY29uc3VtaW5nIGNvbXBvbmVudHMgdG8gc3Vic2NyaWJlIHRvIGNvbnRleHQgY2hhbmdlc1xuZXhwb3J0IGNvbnN0IFVzZXJQcm92aWRlciA9IFVzZXJDb250ZXh0LlByb3ZpZGVyO1xuXG5leHBvcnQgZGVmYXVsdCBVc2VyQ29udGV4dDsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./UserContext.js\n");

/***/ }),

/***/ "./app-helper.js":
/*!***********************!*\
  !*** ./app-helper.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\n  API_URL: process.env.NEXT_PUBLIC_API_URL,\n  getAccessToken: () => localStorage.getItem('token'),\n  toJSON: response => response.json()\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hcHAtaGVscGVyLmpzP2JmN2IiXSwibmFtZXMiOlsibW9kdWxlIiwiZXhwb3J0cyIsIkFQSV9VUkwiLCJwcm9jZXNzIiwiZW52IiwiTkVYVF9QVUJMSUNfQVBJX1VSTCIsImdldEFjY2Vzc1Rva2VuIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsInRvSlNPTiIsInJlc3BvbnNlIiwianNvbiJdLCJtYXBwaW5ncyI6IkFBQUFBLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjtBQUNiQyxTQUFPLEVBQUVDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZQyxtQkFEUjtBQUViQyxnQkFBYyxFQUFFLE1BQU1DLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixDQUZUO0FBR2JDLFFBQU0sRUFBR0MsUUFBRCxJQUFjQSxRQUFRLENBQUNDLElBQVQ7QUFIVCxDQUFqQiIsImZpbGUiOiIuL2FwcC1oZWxwZXIuanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHtcbiAgICBBUElfVVJMOiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19BUElfVVJMLFxuICAgIGdldEFjY2Vzc1Rva2VuOiAoKSA9PiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndG9rZW4nKSxcbiAgICB0b0pTT046IChyZXNwb25zZSkgPT4gcmVzcG9uc2UuanNvbigpXG59XG4iXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./app-helper.js\n");

/***/ }),

/***/ "./pages/login/index.js":
/*!******************************!*\
  !*** ./pages/login/index.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return index; });\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ \"react-bootstrap\");\n/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-google-login */ \"react-google-login\");\n/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_google_login__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ \"sweetalert2\");\n/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _UserContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../UserContext */ \"./UserContext.js\");\n/* harmony import */ var _app_helper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../app-helper */ \"./app-helper.js\");\n/* harmony import */ var _app_helper__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_app_helper__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! next/head */ \"next/head\");\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_7__);\nvar _jsxFileName = \"/Users/PAULA/Documents/B64/capstone3/client/pages/login/index.js\";\n\nvar __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;\n\n\n\n\n\n\n\n\nfunction index() {\n  return __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__[\"Row\"], {\n    className: \"justify-content-center\",\n    __self: this,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 16,\n      columnNumber: 13\n    }\n  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__[\"Col\"], {\n    xs: true,\n    md: \"6\",\n    __self: this,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 17,\n      columnNumber: 17\n    }\n  }, __jsx(\"h3\", {\n    className: \"w-100 text-center d-flex justify-content-center\",\n    __self: this,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 18,\n      columnNumber: 21\n    }\n  }, \"Login\"), __jsx(LoginForm, {\n    __self: this,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 19,\n      columnNumber: 21\n    }\n  })));\n}\n\nconst LoginForm = () => {\n  const {\n    user,\n    setUser\n  } = Object(react__WEBPACK_IMPORTED_MODULE_0__[\"useContext\"])(_UserContext__WEBPACK_IMPORTED_MODULE_5__[\"default\"]);\n  const {\n    0: email,\n    1: setEmail\n  } = Object(react__WEBPACK_IMPORTED_MODULE_0__[\"useState\"])('');\n  const {\n    0: password,\n    1: setPassword\n  } = Object(react__WEBPACK_IMPORTED_MODULE_0__[\"useState\"])('');\n  const {\n    0: tokenId,\n    1: setTokenId\n  } = Object(react__WEBPACK_IMPORTED_MODULE_0__[\"useState\"])(null);\n\n  const authenticate = e => {\n    e.preventDefault();\n    fetch(`http://localhost:4000/api/users/login`, {\n      method: 'POST',\n      headers: {\n        'Content-Type': 'application/json'\n      },\n      body: JSON.stringify({\n        email: email,\n        password: password\n      })\n    }).then(res => res.json()).then(data => {\n      if (data.accessToken) {\n        localStorage.setItem('token', data.accessToken);\n        fetch(`http://localhost:4000/api/users/details`, {\n          headers: {\n            Authorization: `Bearer ${data.accessToken}`\n          }\n        }).then(res => res.json()).then(data => {\n          console.log(data);\n          setUser({\n            id: data._id\n          });\n          next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push('/records');\n        });\n      }\n    });\n  };\n\n  const authenticate2 = e => {\n    e.preventDefault();\n    const payload = {\n      method: 'POST',\n      headers: {\n        'Content-Type': 'application/json'\n      },\n      body: JSON.stringify({\n        email: email,\n        password: password\n      })\n    };\n    fetch(`http://localhost:4000/api/users/login`, payload).then(res => res.json()).then(data => {\n      if (typeof data.accessToken !== 'undefined') {\n        localStorage.setItem('token', data.accessToken);\n        retrieveUserDetails(data.accessToken);\n      } else {\n        if (data.error === 'does-not-exist') {\n          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Authentication Failed', 'User does not exist.', 'error');\n        } else if (data.error === 'incorrect-password') {\n          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Authentication Failed', 'Password is incorrect.', 'error');\n        } else if (data.error === 'login-type-error') {\n          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error');\n        }\n      }\n    });\n  };\n\n  const retrieveUserDetails = accessToken => {\n    const options = {\n      headers: {\n        Authorization: `Bearer ${accessToken}`\n      }\n    };\n    fetch(`http://localhost:4000/api/users/details`, options).then(res => res.json()).then(data => {\n      setUser({\n        email: data.email\n      });\n      next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push('/records');\n    });\n  };\n\n  const authenticateGoogleToken = response => {\n    setTokenId(response.tokenId);\n    const payload = {\n      method: 'post',\n      headers: {\n        'Content-Type': 'application/json'\n      },\n      body: JSON.stringify({\n        tokenId: response.tokenId\n      })\n    };\n    fetch(`http://localhost:4000/api/users/verify-google-token-id`, payload).then(res => res.json()).then(data => {\n      if (typeof data.accessToken !== 'undefined') {\n        localStorage.setItem('token', data.accessToken);\n        retrieveUserDetails(data.accessToken);\n      } else {\n        if (data.error === 'google-auth-error') {\n          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Google Auth Error', 'Google authentication procedure failed, try again or contact web admin.', 'error');\n        } else if (data.error === 'login-type-error') {\n          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error');\n        }\n      }\n    });\n  };\n\n  return __jsx(\"div\", {\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 142,\n      columnNumber: 9\n    }\n  }, __jsx(next_head__WEBPACK_IMPORTED_MODULE_7___default.a, {\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 143,\n      columnNumber: 13\n    }\n  }, __jsx(\"title\", {\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 144,\n      columnNumber: 17\n    }\n  }, \"Login\")), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__[\"Form\"], {\n    onSubmit: e => authenticate(e),\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 146,\n      columnNumber: 13\n    }\n  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__[\"Form\"].Group, {\n    controlId: \"userEmail\",\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 147,\n      columnNumber: 17\n    }\n  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__[\"Form\"].Label, {\n    className: \"w-100 text-center d-flex justify-content-center\",\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 148,\n      columnNumber: 21\n    }\n  }, \"Email address\"), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__[\"Form\"].Control, {\n    type: \"email\",\n    placeholder: \"Enter email\",\n    value: email,\n    onChange: e => setEmail(e.target.value),\n    required: true,\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 149,\n      columnNumber: 21\n    }\n  })), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__[\"Form\"].Group, {\n    controlId: \"password\",\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 152,\n      columnNumber: 17\n    }\n  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__[\"Form\"].Label, {\n    className: \"w-100 text-center d-flex justify-content-center\",\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 153,\n      columnNumber: 21\n    }\n  }, \"Password\"), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__[\"Form\"].Control, {\n    type: \"password\",\n    placeholder: \"XXXXXXXXXXXXXX\",\n    value: password,\n    onChange: e => setPassword(e.target.value),\n    required: true,\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 154,\n      columnNumber: 21\n    }\n  })), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__[\"Button\"], {\n    variant: \"warning\",\n    type: \"submit\",\n    className: \"w-100 text-center d-flex justify-content-center\",\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 157,\n      columnNumber: 17\n    }\n  }, \"Submit\")), __jsx(\"br\", {\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 160,\n      columnNumber: 20\n    }\n  }), __jsx(\"br\", {\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 160,\n      columnNumber: 25\n    }\n  }), __jsx(react_google_login__WEBPACK_IMPORTED_MODULE_2__[\"GoogleLogin\"], {\n    onClick: authenticate2,\n    clientId: \"596826335106-km1g2ceti3s83o1aoclhkld576qm650r.apps.googleusercontent.com\",\n    buttonText: \"Login\",\n    onSuccess: authenticateGoogleToken,\n    onFailure: authenticateGoogleToken,\n    cookiePolicy: 'single_host_origin',\n    className: \"w-100 text-center d-flex justify-content-center\",\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 162,\n      columnNumber: 13\n    }\n  }));\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9wYWdlcy9sb2dpbi9pbmRleC5qcz9jOWY0Il0sIm5hbWVzIjpbImluZGV4IiwiTG9naW5Gb3JtIiwidXNlciIsInNldFVzZXIiLCJ1c2VDb250ZXh0IiwiVXNlckNvbnRleHQiLCJlbWFpbCIsInNldEVtYWlsIiwidXNlU3RhdGUiLCJwYXNzd29yZCIsInNldFBhc3N3b3JkIiwidG9rZW5JZCIsInNldFRva2VuSWQiLCJhdXRoZW50aWNhdGUiLCJlIiwicHJldmVudERlZmF1bHQiLCJmZXRjaCIsIm1ldGhvZCIsImhlYWRlcnMiLCJib2R5IiwiSlNPTiIsInN0cmluZ2lmeSIsInRoZW4iLCJyZXMiLCJqc29uIiwiZGF0YSIsImFjY2Vzc1Rva2VuIiwibG9jYWxTdG9yYWdlIiwic2V0SXRlbSIsIkF1dGhvcml6YXRpb24iLCJjb25zb2xlIiwibG9nIiwiaWQiLCJfaWQiLCJSb3V0ZXIiLCJwdXNoIiwiYXV0aGVudGljYXRlMiIsInBheWxvYWQiLCJyZXRyaWV2ZVVzZXJEZXRhaWxzIiwiZXJyb3IiLCJTd2FsIiwiZmlyZSIsIm9wdGlvbnMiLCJhdXRoZW50aWNhdGVHb29nbGVUb2tlbiIsInJlc3BvbnNlIiwidGFyZ2V0IiwidmFsdWUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUdlLFNBQVNBLEtBQVQsR0FBaUI7QUFFNUIsU0FFUSxNQUFDLG1EQUFEO0FBQUssYUFBUyxFQUFDLHdCQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSSxNQUFDLG1EQUFEO0FBQUssTUFBRSxNQUFQO0FBQVEsTUFBRSxFQUFDLEdBQVg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUksYUFBUyxFQUFDLGlEQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFESixFQUVJLE1BQUMsU0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLElBRkosQ0FESixDQUZSO0FBU0g7O0FBRUQsTUFBTUMsU0FBUyxHQUFHLE1BQU07QUFDcEIsUUFBTTtBQUFFQyxRQUFGO0FBQVFDO0FBQVIsTUFBb0JDLHdEQUFVLENBQUNDLG9EQUFELENBQXBDO0FBRUEsUUFBTTtBQUFBLE9BQUNDLEtBQUQ7QUFBQSxPQUFRQztBQUFSLE1BQW9CQyxzREFBUSxDQUFDLEVBQUQsQ0FBbEM7QUFDQSxRQUFNO0FBQUEsT0FBQ0MsUUFBRDtBQUFBLE9BQVdDO0FBQVgsTUFBMEJGLHNEQUFRLENBQUMsRUFBRCxDQUF4QztBQUNBLFFBQU07QUFBQSxPQUFDRyxPQUFEO0FBQUEsT0FBVUM7QUFBVixNQUF3Qkosc0RBQVEsQ0FBQyxJQUFELENBQXRDOztBQUVBLFFBQU1LLFlBQVksR0FBSUMsQ0FBRCxJQUFPO0FBQ3hCQSxLQUFDLENBQUNDLGNBQUY7QUFFQUMsU0FBSyxDQUFFLHVDQUFGLEVBQTBDO0FBQzNDQyxZQUFNLEVBQUUsTUFEbUM7QUFFM0NDLGFBQU8sRUFBRTtBQUNMLHdCQUFnQjtBQURYLE9BRmtDO0FBSzNDQyxVQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQ2pCZixhQUFLLEVBQUVBLEtBRFU7QUFFakJHLGdCQUFRLEVBQUVBO0FBRk8sT0FBZjtBQUxxQyxLQUExQyxDQUFMLENBVUNhLElBVkQsQ0FVTUMsR0FBRyxJQUFJQSxHQUFHLENBQUNDLElBQUosRUFWYixFQVdDRixJQVhELENBV01HLElBQUksSUFBSTtBQUNWLFVBQUdBLElBQUksQ0FBQ0MsV0FBUixFQUFvQjtBQUNoQkMsb0JBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixFQUE4QkgsSUFBSSxDQUFDQyxXQUFuQztBQUVBVixhQUFLLENBQUUseUNBQUYsRUFBNEM7QUFDN0NFLGlCQUFPLEVBQUU7QUFDTFcseUJBQWEsRUFBRyxVQUFTSixJQUFJLENBQUNDLFdBQVk7QUFEckM7QUFEb0MsU0FBNUMsQ0FBTCxDQUtDSixJQUxELENBS01DLEdBQUcsSUFBSUEsR0FBRyxDQUFDQyxJQUFKLEVBTGIsRUFNQ0YsSUFORCxDQU1NRyxJQUFJLElBQUk7QUFDVkssaUJBQU8sQ0FBQ0MsR0FBUixDQUFZTixJQUFaO0FBQ0F0QixpQkFBTyxDQUFDO0FBQ0o2QixjQUFFLEVBQUVQLElBQUksQ0FBQ1E7QUFETCxXQUFELENBQVA7QUFHQUMsNERBQU0sQ0FBQ0MsSUFBUCxDQUFZLFVBQVo7QUFDSCxTQVpEO0FBYUg7QUFDSixLQTdCRDtBQThCSCxHQWpDRDs7QUFtQ0EsUUFBTUMsYUFBYSxHQUFJdEIsQ0FBRCxJQUFPO0FBQ3pCQSxLQUFDLENBQUNDLGNBQUY7QUFFQSxVQUFNc0IsT0FBTyxHQUFHO0FBQ1pwQixZQUFNLEVBQUUsTUFESTtBQUVaQyxhQUFPLEVBQUU7QUFDTCx3QkFBZ0I7QUFEWCxPQUZHO0FBS1pDLFVBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWU7QUFDakJmLGFBQUssRUFBRUEsS0FEVTtBQUVqQkcsZ0JBQVEsRUFBRUE7QUFGTyxPQUFmO0FBTE0sS0FBaEI7QUFXQU8sU0FBSyxDQUFFLHVDQUFGLEVBQTBDcUIsT0FBMUMsQ0FBTCxDQUNDZixJQURELENBQ01DLEdBQUcsSUFBSUEsR0FBRyxDQUFDQyxJQUFKLEVBRGIsRUFFQ0YsSUFGRCxDQUVNRyxJQUFJLElBQUk7QUFDVixVQUFJLE9BQU9BLElBQUksQ0FBQ0MsV0FBWixLQUE0QixXQUFoQyxFQUE2QztBQUN6Q0Msb0JBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixFQUE4QkgsSUFBSSxDQUFDQyxXQUFuQztBQUNBWSwyQkFBbUIsQ0FBQ2IsSUFBSSxDQUFDQyxXQUFOLENBQW5CO0FBQ0gsT0FIRCxNQUdPO0FBQ0gsWUFBSUQsSUFBSSxDQUFDYyxLQUFMLEtBQWUsZ0JBQW5CLEVBQXFDO0FBQ2pDQyw0REFBSSxDQUFDQyxJQUFMLENBQVUsdUJBQVYsRUFBbUMsc0JBQW5DLEVBQTJELE9BQTNEO0FBQ0gsU0FGRCxNQUVPLElBQUloQixJQUFJLENBQUNjLEtBQUwsS0FBZSxvQkFBbkIsRUFBeUM7QUFDNUNDLDREQUFJLENBQUNDLElBQUwsQ0FBVSx1QkFBVixFQUFtQyx3QkFBbkMsRUFBNkQsT0FBN0Q7QUFDSCxTQUZNLE1BRUEsSUFBSWhCLElBQUksQ0FBQ2MsS0FBTCxLQUFlLGtCQUFuQixFQUF1QztBQUMxQ0MsNERBQUksQ0FBQ0MsSUFBTCxDQUFVLGtCQUFWLEVBQThCLGdHQUE5QixFQUFnSSxPQUFoSTtBQUNIO0FBQ0o7QUFDSixLQWZEO0FBZ0JILEdBOUJEOztBQWdDQSxRQUFNSCxtQkFBbUIsR0FBSVosV0FBRCxJQUFpQjtBQUN6QyxVQUFNZ0IsT0FBTyxHQUFHO0FBQ1p4QixhQUFPLEVBQUU7QUFBRVcscUJBQWEsRUFBRyxVQUFVSCxXQUFhO0FBQXpDO0FBREcsS0FBaEI7QUFJQVYsU0FBSyxDQUFFLHlDQUFGLEVBQTRDMEIsT0FBNUMsQ0FBTCxDQUNDcEIsSUFERCxDQUNNQyxHQUFHLElBQUlBLEdBQUcsQ0FBQ0MsSUFBSixFQURiLEVBRUNGLElBRkQsQ0FFTUcsSUFBSSxJQUFJO0FBQ1Z0QixhQUFPLENBQUM7QUFBRUcsYUFBSyxFQUFFbUIsSUFBSSxDQUFDbkI7QUFBZCxPQUFELENBQVA7QUFDQTRCLHdEQUFNLENBQUNDLElBQVAsQ0FBWSxVQUFaO0FBQ0gsS0FMRDtBQU1ILEdBWEQ7O0FBYUEsUUFBTVEsdUJBQXVCLEdBQUlDLFFBQUQsSUFBYztBQUUxQ2hDLGNBQVUsQ0FBQ2dDLFFBQVEsQ0FBQ2pDLE9BQVYsQ0FBVjtBQUVBLFVBQU0wQixPQUFPLEdBQUc7QUFDWnBCLFlBQU0sRUFBRSxNQURJO0FBRVpDLGFBQU8sRUFBRTtBQUFFLHdCQUFnQjtBQUFsQixPQUZHO0FBSVpDLFVBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWU7QUFDakJWLGVBQU8sRUFBRWlDLFFBQVEsQ0FBQ2pDO0FBREQsT0FBZjtBQUpNLEtBQWhCO0FBU0FLLFNBQUssQ0FBRSx3REFBRixFQUEyRHFCLE9BQTNELENBQUwsQ0FDQ2YsSUFERCxDQUNNQyxHQUFHLElBQUlBLEdBQUcsQ0FBQ0MsSUFBSixFQURiLEVBRUNGLElBRkQsQ0FFTUcsSUFBSSxJQUFJO0FBQ1YsVUFBSSxPQUFPQSxJQUFJLENBQUNDLFdBQVosS0FBNEIsV0FBaEMsRUFBNkM7QUFDekNDLG9CQUFZLENBQUNDLE9BQWIsQ0FBcUIsT0FBckIsRUFBOEJILElBQUksQ0FBQ0MsV0FBbkM7QUFDQVksMkJBQW1CLENBQUNiLElBQUksQ0FBQ0MsV0FBTixDQUFuQjtBQUNILE9BSEQsTUFHTztBQUNILFlBQUlELElBQUksQ0FBQ2MsS0FBTCxLQUFlLG1CQUFuQixFQUF3QztBQUNwQ0MsNERBQUksQ0FBQ0MsSUFBTCxDQUFVLG1CQUFWLEVBQStCLHlFQUEvQixFQUEwRyxPQUExRztBQUNILFNBRkQsTUFFTyxJQUFJaEIsSUFBSSxDQUFDYyxLQUFMLEtBQWUsa0JBQW5CLEVBQXVDO0FBQzFDQyw0REFBSSxDQUFDQyxJQUFMLENBQVUsa0JBQVYsRUFBOEIsZ0dBQTlCLEVBQWdJLE9BQWhJO0FBQ0g7QUFDSjtBQUNKLEtBYkQ7QUFjSCxHQTNCRDs7QUE2QkEsU0FDSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0ksTUFBQyxnREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQURKLENBREosRUFJSSxNQUFDLG9EQUFEO0FBQU0sWUFBUSxFQUFHM0IsQ0FBRCxJQUFPRCxZQUFZLENBQUNDLENBQUQsQ0FBbkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJLE1BQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVksYUFBUyxFQUFDLFdBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSSxNQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLGFBQVMsRUFBQyxpREFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFESixFQUVJLE1BQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQWMsUUFBSSxFQUFDLE9BQW5CO0FBQTJCLGVBQVcsRUFBQyxhQUF2QztBQUFxRCxTQUFLLEVBQUVSLEtBQTVEO0FBQW1FLFlBQVEsRUFBR1EsQ0FBRCxJQUFPUCxRQUFRLENBQUNPLENBQUMsQ0FBQytCLE1BQUYsQ0FBU0MsS0FBVixDQUE1RjtBQUE4RyxZQUFRLE1BQXRIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsSUFGSixDQURKLEVBTUksTUFBQyxvREFBRCxDQUFNLEtBQU47QUFBWSxhQUFTLEVBQUMsVUFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJLE1BQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVksYUFBUyxFQUFDLGlEQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURKLEVBRUksTUFBQyxvREFBRCxDQUFNLE9BQU47QUFBYyxRQUFJLEVBQUMsVUFBbkI7QUFBOEIsZUFBVyxFQUFDLGdCQUExQztBQUEyRCxTQUFLLEVBQUVyQyxRQUFsRTtBQUE0RSxZQUFRLEVBQUdLLENBQUQsSUFBT0osV0FBVyxDQUFDSSxDQUFDLENBQUMrQixNQUFGLENBQVNDLEtBQVYsQ0FBeEc7QUFBMEgsWUFBUSxNQUFsSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLElBRkosQ0FOSixFQVdJLE1BQUMsc0RBQUQ7QUFBUSxXQUFPLEVBQUMsU0FBaEI7QUFBMEIsUUFBSSxFQUFDLFFBQS9CO0FBQXdDLGFBQVMsRUFBQyxpREFBbEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQVhKLENBSkosRUFrQlc7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQWxCWCxFQWtCZ0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQWxCaEIsRUFvQkksTUFBQyw4REFBRDtBQUFhLFdBQU8sRUFBR1YsYUFBdkI7QUFDSSxZQUFRLEVBQUMsMEVBRGI7QUFFSSxjQUFVLEVBQUMsT0FGZjtBQUdJLGFBQVMsRUFBR08sdUJBSGhCO0FBSUksYUFBUyxFQUFHQSx1QkFKaEI7QUFLSSxnQkFBWSxFQUFHLG9CQUxuQjtBQU1JLGFBQVMsRUFBQyxpREFOZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLElBcEJKLENBREo7QUFnQ0gsQ0FwSkQiLCJmaWxlIjoiLi9wYWdlcy9sb2dpbi9pbmRleC5qcy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHVzZVN0YXRlLCB1c2VDb250ZXh0IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgRm9ybSwgQnV0dG9uLCBDYXJkLCBSb3csIENvbCB9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCdcbmltcG9ydCB7IEdvb2dsZUxvZ2luIH0gZnJvbSAncmVhY3QtZ29vZ2xlLWxvZ2luJ1xuaW1wb3J0IFJvdXRlciBmcm9tICduZXh0L3JvdXRlcidcbmltcG9ydCBTd2FsIGZyb20gJ3N3ZWV0YWxlcnQyJ1xuaW1wb3J0IFVzZXJDb250ZXh0IGZyb20gJy4uLy4uL1VzZXJDb250ZXh0JztcbmltcG9ydCBBcHBIZWxwZXIgZnJvbSAnLi4vLi4vYXBwLWhlbHBlcidcblxuaW1wb3J0IEhlYWQgZnJvbSAnbmV4dC9oZWFkJ1xuXG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGluZGV4KCkge1xuXG4gICAgcmV0dXJuIChcbiAgICAgICBcbiAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LWNlbnRlclwiPlxuICAgICAgICAgICAgICAgIDxDb2wgeHMgbWQ9XCI2XCI+XG4gICAgICAgICAgICAgICAgICAgIDxoMyBjbGFzc05hbWU9XCJ3LTEwMCB0ZXh0LWNlbnRlciBkLWZsZXgganVzdGlmeS1jb250ZW50LWNlbnRlclwiPkxvZ2luPC9oMz5cbiAgICAgICAgICAgICAgICAgICAgPExvZ2luRm9ybS8+XG4gICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICA8L1Jvdz5cbiAgICApXG59XG5cbmNvbnN0IExvZ2luRm9ybSA9ICgpID0+IHtcbiAgICBjb25zdCB7IHVzZXIsIHNldFVzZXIgfSA9IHVzZUNvbnRleHQoVXNlckNvbnRleHQpXG5cbiAgICBjb25zdCBbZW1haWwsIHNldEVtYWlsXSA9IHVzZVN0YXRlKCcnKVxuICAgIGNvbnN0IFtwYXNzd29yZCwgc2V0UGFzc3dvcmRdID0gdXNlU3RhdGUoJycpXG4gICAgY29uc3QgW3Rva2VuSWQsIHNldFRva2VuSWRdID0gdXNlU3RhdGUobnVsbClcblxuICAgIGNvbnN0IGF1dGhlbnRpY2F0ZSA9IChlKSA9PiB7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKVxuXG4gICAgICAgIGZldGNoKGBodHRwOi8vbG9jYWxob3N0OjQwMDAvYXBpL3VzZXJzL2xvZ2luYCwge1xuICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHtcbiAgICAgICAgICAgICAgICBlbWFpbDogZW1haWwsXG4gICAgICAgICAgICAgICAgcGFzc3dvcmQ6IHBhc3N3b3JkLFxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSlcbiAgICAgICAgLnRoZW4ocmVzID0+IHJlcy5qc29uKCkpXG4gICAgICAgIC50aGVuKGRhdGEgPT4ge1xuICAgICAgICAgICAgaWYoZGF0YS5hY2Nlc3NUb2tlbil7XG4gICAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3Rva2VuJywgZGF0YS5hY2Nlc3NUb2tlbik7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgZmV0Y2goYGh0dHA6Ly9sb2NhbGhvc3Q6NDAwMC9hcGkvdXNlcnMvZGV0YWlsc2AsIHtcbiAgICAgICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2RhdGEuYWNjZXNzVG9rZW59YFxuICAgICAgICAgICAgICAgICAgICB9IFxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzID0+IHJlcy5qc29uKCkpXG4gICAgICAgICAgICAgICAgLnRoZW4oZGF0YSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgICAgICAgICAgICAgICAgIHNldFVzZXIoe1xuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IGRhdGEuX2lkXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIFJvdXRlci5wdXNoKCcvcmVjb3JkcycpXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBjb25zdCBhdXRoZW50aWNhdGUyID0gKGUpID0+IHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpXG5cbiAgICAgICAgY29uc3QgcGF5bG9hZCA9IHtcbiAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgICAgICAgaGVhZGVyczogeyBcbiAgICAgICAgICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nICBcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7IFxuICAgICAgICAgICAgICAgIGVtYWlsOiBlbWFpbCwgXG4gICAgICAgICAgICAgICAgcGFzc3dvcmQ6IHBhc3N3b3JkIFxuICAgICAgICAgICAgfSlcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgZmV0Y2goYGh0dHA6Ly9sb2NhbGhvc3Q6NDAwMC9hcGkvdXNlcnMvbG9naW5gLCBwYXlsb2FkKVxuICAgICAgICAudGhlbihyZXMgPT4gcmVzLmpzb24oKSlcbiAgICAgICAgLnRoZW4oZGF0YSA9PiB7XG4gICAgICAgICAgICBpZiAodHlwZW9mIGRhdGEuYWNjZXNzVG9rZW4gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3Rva2VuJywgZGF0YS5hY2Nlc3NUb2tlbilcbiAgICAgICAgICAgICAgICByZXRyaWV2ZVVzZXJEZXRhaWxzKGRhdGEuYWNjZXNzVG9rZW4pXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9yID09PSAnZG9lcy1ub3QtZXhpc3QnKSB7XG4gICAgICAgICAgICAgICAgICAgIFN3YWwuZmlyZSgnQXV0aGVudGljYXRpb24gRmFpbGVkJywgJ1VzZXIgZG9lcyBub3QgZXhpc3QuJywgJ2Vycm9yJylcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGRhdGEuZXJyb3IgPT09ICdpbmNvcnJlY3QtcGFzc3dvcmQnKSB7XG4gICAgICAgICAgICAgICAgICAgIFN3YWwuZmlyZSgnQXV0aGVudGljYXRpb24gRmFpbGVkJywgJ1Bhc3N3b3JkIGlzIGluY29ycmVjdC4nLCAnZXJyb3InKVxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZGF0YS5lcnJvciA9PT0gJ2xvZ2luLXR5cGUtZXJyb3InKSB7XG4gICAgICAgICAgICAgICAgICAgIFN3YWwuZmlyZSgnTG9naW4gVHlwZSBFcnJvcicsICdZb3UgbWF5IGhhdmUgcmVnaXN0ZXJlZCB0aHJvdWdoIGEgZGlmZmVyZW50IGxvZ2luIHByb2NlZHVyZSwgdHJ5IGFsdGVybmF0aXZlIGxvZ2luIHByb2NlZHVyZXMuJywgJ2Vycm9yJylcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgfVxuICAgIFxuICAgIGNvbnN0IHJldHJpZXZlVXNlckRldGFpbHMgPSAoYWNjZXNzVG9rZW4pID0+IHtcbiAgICAgICAgY29uc3Qgb3B0aW9ucyA9IHtcbiAgICAgICAgICAgIGhlYWRlcnM6IHsgQXV0aG9yaXphdGlvbjogYEJlYXJlciAkeyBhY2Nlc3NUb2tlbiB9YCB9IFxuICAgICAgICB9XG5cbiAgICAgICAgZmV0Y2goYGh0dHA6Ly9sb2NhbGhvc3Q6NDAwMC9hcGkvdXNlcnMvZGV0YWlsc2AsIG9wdGlvbnMpXG4gICAgICAgIC50aGVuKHJlcyA9PiByZXMuanNvbigpKVxuICAgICAgICAudGhlbihkYXRhID0+IHtcbiAgICAgICAgICAgIHNldFVzZXIoeyBlbWFpbDogZGF0YS5lbWFpbCB9KVxuICAgICAgICAgICAgUm91dGVyLnB1c2goJy9yZWNvcmRzJylcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBjb25zdCBhdXRoZW50aWNhdGVHb29nbGVUb2tlbiA9IChyZXNwb25zZSkgPT4ge1xuXG4gICAgICAgIHNldFRva2VuSWQocmVzcG9uc2UudG9rZW5JZClcblxuICAgICAgICBjb25zdCBwYXlsb2FkID0ge1xuICAgICAgICAgICAgbWV0aG9kOiAncG9zdCcsXG4gICAgICAgICAgICBoZWFkZXJzOiB7ICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicgXG4gICAgICAgIH0sXG4gICAgICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7IFxuICAgICAgICAgICAgICAgIHRva2VuSWQ6IHJlc3BvbnNlLnRva2VuSWQgXG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG5cbiAgICAgICAgZmV0Y2goYGh0dHA6Ly9sb2NhbGhvc3Q6NDAwMC9hcGkvdXNlcnMvdmVyaWZ5LWdvb2dsZS10b2tlbi1pZGAsIHBheWxvYWQpXG4gICAgICAgIC50aGVuKHJlcyA9PiByZXMuanNvbigpKVxuICAgICAgICAudGhlbihkYXRhID0+IHtcbiAgICAgICAgICAgIGlmICh0eXBlb2YgZGF0YS5hY2Nlc3NUb2tlbiAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgndG9rZW4nLCBkYXRhLmFjY2Vzc1Rva2VuKVxuICAgICAgICAgICAgICAgIHJldHJpZXZlVXNlckRldGFpbHMoZGF0YS5hY2Nlc3NUb2tlbilcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3IgPT09ICdnb29nbGUtYXV0aC1lcnJvcicpIHtcbiAgICAgICAgICAgICAgICAgICAgU3dhbC5maXJlKCdHb29nbGUgQXV0aCBFcnJvcicsICdHb29nbGUgYXV0aGVudGljYXRpb24gcHJvY2VkdXJlIGZhaWxlZCwgdHJ5IGFnYWluIG9yIGNvbnRhY3Qgd2ViIGFkbWluLicsICdlcnJvcicpXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChkYXRhLmVycm9yID09PSAnbG9naW4tdHlwZS1lcnJvcicpIHtcbiAgICAgICAgICAgICAgICAgICAgU3dhbC5maXJlKCdMb2dpbiBUeXBlIEVycm9yJywgJ1lvdSBtYXkgaGF2ZSByZWdpc3RlcmVkIHRocm91Z2ggYSBkaWZmZXJlbnQgbG9naW4gcHJvY2VkdXJlLCB0cnkgYWx0ZXJuYXRpdmUgbG9naW4gcHJvY2VkdXJlcy4nLCAnZXJyb3InKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPEhlYWQ+XG4gICAgICAgICAgICAgICAgPHRpdGxlPkxvZ2luPC90aXRsZT5cbiAgICAgICAgICAgIDwvSGVhZD5cbiAgICAgICAgICAgIDxGb3JtIG9uU3VibWl0PXsoZSkgPT4gYXV0aGVudGljYXRlKGUpfT5cbiAgICAgICAgICAgICAgICA8Rm9ybS5Hcm91cCBjb250cm9sSWQ9XCJ1c2VyRW1haWxcIj5cbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uTGFiZWwgY2xhc3NOYW1lPVwidy0xMDAgdGV4dC1jZW50ZXIgZC1mbGV4IGp1c3RpZnktY29udGVudC1jZW50ZXJcIj5FbWFpbCBhZGRyZXNzPC9Gb3JtLkxhYmVsPlxuICAgICAgICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sIHR5cGU9XCJlbWFpbFwiIHBsYWNlaG9sZGVyPVwiRW50ZXIgZW1haWxcIiB2YWx1ZT17ZW1haWx9IG9uQ2hhbmdlPXsoZSkgPT4gc2V0RW1haWwoZS50YXJnZXQudmFsdWUpfSByZXF1aXJlZC8+XG4gICAgICAgICAgICAgICAgPC9Gb3JtLkdyb3VwPlxuXG4gICAgICAgICAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwicGFzc3dvcmRcIj5cbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uTGFiZWwgY2xhc3NOYW1lPVwidy0xMDAgdGV4dC1jZW50ZXIgZC1mbGV4IGp1c3RpZnktY29udGVudC1jZW50ZXJcIj5QYXNzd29yZDwvRm9ybS5MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbCB0eXBlPVwicGFzc3dvcmRcIiBwbGFjZWhvbGRlcj1cIlhYWFhYWFhYWFhYWFhYXCIgdmFsdWU9e3Bhc3N3b3JkfSBvbkNoYW5nZT17KGUpID0+IHNldFBhc3N3b3JkKGUudGFyZ2V0LnZhbHVlKX0gcmVxdWlyZWQvPlxuICAgICAgICAgICAgICAgIDwvRm9ybS5Hcm91cD5cblxuICAgICAgICAgICAgICAgIDxCdXR0b24gdmFyaWFudD1cIndhcm5pbmdcIiB0eXBlPVwic3VibWl0XCIgY2xhc3NOYW1lPVwidy0xMDAgdGV4dC1jZW50ZXIgZC1mbGV4IGp1c3RpZnktY29udGVudC1jZW50ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgU3VibWl0XG4gICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICA8L0Zvcm0+PGJyLz48YnIvPlxuICAgICAgICAgICAgXG4gICAgICAgICAgICA8R29vZ2xlTG9naW4gb25DbGljaz17IGF1dGhlbnRpY2F0ZTIgfVxuICAgICAgICAgICAgICAgIGNsaWVudElkPVwiNTk2ODI2MzM1MTA2LWttMWcyY2V0aTNzODNvMWFvY2xoa2xkNTc2cW02NTByLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tXCJcbiAgICAgICAgICAgICAgICBidXR0b25UZXh0PVwiTG9naW5cIlxuICAgICAgICAgICAgICAgIG9uU3VjY2Vzcz17IGF1dGhlbnRpY2F0ZUdvb2dsZVRva2VuIH1cbiAgICAgICAgICAgICAgICBvbkZhaWx1cmU9eyBhdXRoZW50aWNhdGVHb29nbGVUb2tlbiB9XG4gICAgICAgICAgICAgICAgY29va2llUG9saWN5PXsgJ3NpbmdsZV9ob3N0X29yaWdpbicgfVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInctMTAwIHRleHQtY2VudGVyIGQtZmxleCBqdXN0aWZ5LWNvbnRlbnQtY2VudGVyXCJcbiAgICAgICAgICAgIC8+XG4gICAgXG4gICAgICAgIDwvZGl2PlxuICAgIClcbn1cblxuICAgICAgICBcbiAgICAgICAgIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/login/index.js\n");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"next/head\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJuZXh0L2hlYWRcIj81ZWYyIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBIiwiZmlsZSI6Im5leHQvaGVhZC5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQvaGVhZFwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///next/head\n");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"next/router\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJuZXh0L3JvdXRlclwiP2Q4M2UiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEiLCJmaWxlIjoibmV4dC9yb3V0ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0L3JvdXRlclwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///next/router\n");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiPzU4OGUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEiLCJmaWxlIjoicmVhY3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///react\n");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-bootstrap\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1ib290c3RyYXBcIj8zODUwIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBIiwiZmlsZSI6InJlYWN0LWJvb3RzdHJhcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWJvb3RzdHJhcFwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///react-bootstrap\n");

/***/ }),

/***/ "react-google-login":
/*!*************************************!*\
  !*** external "react-google-login" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-google-login\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1nb29nbGUtbG9naW5cIj9kZDQ2Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBIiwiZmlsZSI6InJlYWN0LWdvb2dsZS1sb2dpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWdvb2dsZS1sb2dpblwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///react-google-login\n");

/***/ }),

/***/ "sweetalert2":
/*!******************************!*\
  !*** external "sweetalert2" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"sweetalert2\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJzd2VldGFsZXJ0MlwiP2MyZjUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEiLCJmaWxlIjoic3dlZXRhbGVydDIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJzd2VldGFsZXJ0MlwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///sweetalert2\n");

/***/ })

/******/ });